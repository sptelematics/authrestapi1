asgiref==3.2.7
astroid==2.4.1
certifi==2020.4.5.1
cffi==1.14.0
chardet==3.0.4
click==7.1.2
colorama==0.4.3
coreapi==2.3.3
coreschema==0.0.4
cryptography==2.9.2
defusedxml==0.7.0rc1
dj-database-url==0.5.0
Django==3.0.5
django-allauth==0.42.0
django-cors-headers==3.3.0
django-filter==2.3.0
django-multiselectfield==0.1.12
django-rest-auth==0.9.5
django-rest-swagger==2.0.7
djangorestframework==3.11.0
dnspython==1.16.0
gunicorn==20.0.4
heroku==0.1.4
idna==2.9
isort==4.3.21
itsdangerous==1.1.0
itypes==1.2.0
Jinja2==2.11.2
lazy-object-proxy==1.4.3
Markdown==3.2.2
MarkupSafe==1.1.1
mccabe==0.6.1
mysql-client==0.0.1
mysql-connector-python==8.0.20
oauthlib==3.1.0
openapi-codec==1.3.2
Pillow==7.1.2
protobuf==3.6.1
psycopg2==2.8.5
pycparser==2.20
PyJWT==1.7.1
pylint==2.5.0
PyMySQL==0.9.3
python-dateutil==1.5
python-social-auth==0.3.6
python3-openid==3.1.0
pytz==2019.3
PyYAML==5.3.1
requests==2.23.0
requests-oauthlib==1.3.0
simplejson==3.17.0
six==1.15.0
social-auth-app-django==3.4.0
social-auth-core==3.3.3
sqlparse==0.3.1
svgwrite==1.4
toml==0.10.0
Tree==0.2.4
uritemplate==3.0.1
urllib3==1.25.9
virtualenv==16.7.8
virtualenvwrapper-win==1.2.5
Werkzeug==1.0.1
whitenoise==5.0.1
wrapt==1.12.1
